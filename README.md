## dotfiles for STOW

This repository contains a [GNU stow](https://www.gnu.org/software/stow/) directory structure intended to [manage various dotfiles](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html).


## Usage


```bash
cd /path/to/this/repository

# install freeplane
stow freeplane -t $HOME
```


## Contents


### freeplane

- Last updated while using [Freeplane](http://www.freeplane.org/wiki/index.php/Main_Page) version 1.5.14.
- Installed add-ons:
  - The [GTK+ 1.8.2](https://github.com/gpapp/FreePlaneGTD/)
- Includes a `freeplane.desktop` file that assumes `freeplane.sh` is installed under `$HOME/bin/freeplane/`

### ssh

- The `~/.ssh/config` file will include additional customizations any files
  matching the path `~/.ssh/config.d/*.ssh-config`.
- If you want to include local customizations, make sure `~/.ssh/config.d` is a local directory.
  - For example, you can run `mkdir -p ~/.ssh/config.d` before running `stow`
    the first time.

### vim

The `.vimrc` file:

- uses Plug to manage vim  plugins
- will attempt to create and use a separate directory for swapfiles (default:
  `$HOME/.vim/swapfiles/`)

### xdg

Installs various xdg mime types, .desktop files, and helper scripts:

- Handle Windows-style `.url` shortcut files with local web browser
