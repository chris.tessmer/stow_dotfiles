set nocompatible              " be iMproved, required
"runtime macros/matchit.vim

" ------------------------------------------------------------------------------
" Plugins
" ------------------------------------------------------------------------------
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')

" the BEST git integration for vim
Plug 'tpope/vim-fugitive'

" vim IDE
Plug 'vim-scripts/project.tar.gz'

" vim Wiki w/lurvely checklists and wiki syntax, killer combo with Project
Plug 'vimwiki/vimwiki'

" pretty pretty status bar; integrates with other services
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Syntax and aesthetic plugins
" ----------------------------------------

" Syntax highlighting for markdown
"   See: https://www.reddit.com/r/vim/comments/3yl70y/what_markdown_plugin_is_everyone_using/
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'

"Provides `:GenTocGFM` command to auto-generate markdown TOCs
Plug 'mzlogin/vim-markdown-toc'

" Syntax highlighting for markdown
"
"Plug 'plasticboy/vim-markdown'
"
"" Syntax highlighting for Github-flavored markdown
" Plug 'https://github.com/tpope/vim-markdown'

" syntax highlighting and folding for Windows PowerShell
Plug 'PProvost/vim-ps1'

Plug 'evansalter/vim-checklist'
Plug 'vim-syntastic/syntastic'

" Color themes
" ----------------------------------------

" reduces eye-strain AND looks trendy
Plug 'altercation/vim-colors-solarized'

" enables the :SCROLLCOLOR command
Plug 'vim-scripts/ScrollColors'

" auto-formatting for Puppet
Plug 'rodjek/vim-puppet'

" Lovely whitespace alignment
Plug 'godlygeek/tabular'

" Makes ASCII tables easy (for ReST, etc)
Plug 'dhruvasagar/vim-table-mode'

Plug 'vim-scripts/neverland.vim--All-colorschemes-suck'
Plug 'chriskempson/base16-vim'

" Display your undo history in a graph
" NOTE: requires vim >= 7.3
Plug 'mbbill/undotree'

Plug 'vim-scripts/surround.vim'

" Jump to the beginning/end of blocks with the '%' key
Plug 'vim-scripts/matchit.zip'


" Plugins I'm playing with
" ----------------------------------------

" Use vim as the man pager; it even follows man page links!
"
"    export MANPAGER="/usr/bin/vim -M +MANPAGER -"
"
Plug 'lambdalisue/vim-manpager'

""" TextMate-like snippets for vim!
""" Plug 'garbas/vim-snipmate'
""" Plug 'msanders/snipmate.vim' (now garbas/vim-snipmate)
""" Plug 'MarcWeber/vim-addon-mw-utils'
""" Plug 'tomtom/tlib_vim'
""" Plug 'honza/vim-snippets'



" ASCII-art box-drawing plugin
"    https://github.com/gyim/vim-boxdraw
" Plug 'gyim/vim-boxdraw'

" NOTE: read prerequisite instructions carefully at https://github.com/tbabej/taskwiki
""" Plug 'tbabej/taskwiki'


""" Plug 'powerman/vim-plugin-AnsiEsc'
""" Plug 'majutsushi/tagbar'
""" Plug 'farseer90718/vim-taskwarrior'


""""""""" todos & outlining
"Plug 'fmoralesc/vim-pad'
"Plug 'vitalk/vim-simple-todo'
"Plug 'vimoutliner/vimoutliner'


"""""""""
""" TODO: check these out
"Plug 'q335r49/microviche'
""" find ANYTHING, quickly
" Plug 'Shougo/unite.vim'
""" fuzzy file finding (<-- and awesomely accessible alliteration!)
"Plug 'ctrlp.vim'

" All of your Plugins must be added before the following line
call plug#end()              " required

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plug stuff after this line

" ------------------------------------------------------------------------------
" Plug settings
" ------------------------------------------------------------------------------

" vimproject
" --------------------------------------
" g = <F12> toggles Project sidebar
let g:proj_flags="imstbgv"


" vimwiki
" --------------------------------------
let wiki_notes = {}
let wiki_notes.path = '~/notes/'
let wiki_notes.html_header = '~/public_html/header.tpl'
let wiki_notes.nested_syntaxes = {'ruby': 'ruby', 'puppet': 'puppet', 'yaml': 'yaml', 'bash': 'sh', 'sh': 'sh', 'python': 'python', 'c++': 'cpp', 'erb': 'eruby', 'eruby': 'eruby'}
let wiki_notes.syntax = 'markdown'
let wiki_notes.ext = '.md'

" Example additional wiki
"let wiki_2 = {}
"let wiki_2.path = '~/another_wiki/'
"let wiki_2.index = 'main'

" list of active wikis
let g:vimwiki_list = [wiki_notes]

"
let g:table_mode_corner = '+'
let g:table_mode_separator = '|'

" this is the best characters for the default Monospace font
let g:airline_left_sep='▶'
let g:airline_right_sep='◀'
let g:airline_theme='bubblegum'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:pandoc#modules#disabled = ["folding"]
let g:pandoc#syntax#codeblocks#embeds#langs = ["ruby","puppet","bash=sh","json=javascript"]
let g:pandoc#syntax#conceal#use = 0
let g:pandoc#syntax#conceal#urls = 0

let vim_markdown_preview_github=1
"let vim_markdown_preview_toggle=2
"let vim_markdown_preview_use_xdg_open=1

" crtlp
" --------------------------------------
let g:ctrlp_regexp = 1

" vim-notes
" --------------------------------------
:let g:notes_directories = ['~/Documents/notes', '~/notes', '~/.notes']

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let b:syntastic_mode = 'passive'
let g:syntastic_sh_shellcheck_args = "-x -a -e SC2207"


" ------------------------------------------------------------------------------
" General settings
" ------------------------------------------------------------------------------
syntax on
set tabstop=2 shiftwidth=2 expandtab  " make tabs spaces
set autoindent
set ruler



" key bindings
" --------------------------------------
"  toggle paste mode (prevents wonky formatting when pasting into a terminal)
map <F2> :set paste!<cr>

" remove whitespace
map <F4> :%s/\s\+$//<cr>

" toggle spellcheck
map <F7> :set spell! spelllang=en_us spellfile=~/.vim/spellfile.add<cr>

" toggle the undotreeplugin
nnoremap <F5> :UndotreeToggle<cr>

" align values in simple YAML
map <F8> :Tabularize /:\zs <cr>

" fold options
" --------------------------------------

" Show trailing whitespace and spaces before a tab:
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
highlight ExtraWhitespace ctermbg=red guibg=red
:autocmd BufWinEnter * match ExtraWhitespace /\s\+$\| \+\ze\t/
:au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
:au InsertLeave * match ExtraWhitespace /\s\+$/


" custom colors
" --------------------------------------
" colors for vimdiff
" from https://gforge.onyxpoint.net/redmine/projects/simp-dev/wiki/Tips_and_Tricks
if &diff
  set t_Co=256
  colorscheme zellner
endif


set t_Co=256
set background=dark
"let g:solarized_termcolors=256
"colorscheme solarized  " NOTE: this interferes with base16 gnome colors

"autocmd BufNewFile,BufReadPost *.md set filetype=markdown
if &term =~ '^screen'
    " tmux will send xterm-style keys when its xterm-keys option is on
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif

autocmd BufNewFile,BufReadPost *.md set filetype=markdown
augroup markdown
    au!
    au BufNewFile,BufRead *.md,*.markdown setlocal filetype=ghmarkdown
augroup END


let g:markdown_fenced_languages = ['html', 'python', 'bash=sh', 'puppet', 'ruby', 'eruby']

" expand all folds by default
"   See: http://stackoverflow.com/a/23672376
autocmd BufWinEnter * silent! :%foldopen!


" Make a directory if it doesn't already exist
" --------------------------------------
" from: https://stackoverflow.com/a/8462159
function! EnsureDirExists (dir)
  if !isdirectory(a:dir)
    if exists("*mkdir")
      call mkdir(a:dir,'p')
      echo "Created directory: " . a:dir
    else
      echo "Please create directory: " . a:dir
    endif
  endif
endfunction


" keep all swapfiles in a single directory
" --------------------------------------
" create directory for swapfiles if it doesn't exist
if isdirectory('/opt/ctessmer')
  let vim_backup_dir = '/opt/ctessmer/.vim/swapfiles//'
else
  let vim_backup_dir = $HOME . '/.vim/swapfiles//'
endif
call EnsureDirExists(vim_backup_dir)

" https://stackoverflow.com/a/21026618
let &directory = vim_backup_dir

