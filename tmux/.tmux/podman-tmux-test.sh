#!/bin/bash
# Test tmux.conf from various OSes
# Run from .tmux directory (.tmux.conf assumed to be in parent directory)

image="${1:-centos:7}"

echo "Using image '$image.."
printf "Try testing with:\n\n\t$0 centos:7\n\t$0 centos:8\n\t$0 fedora:35\n\n"

# NOTE: using --security-opt label=disable instead of mounting with :Z
#       See https://danwalsh.livejournal.com/81143.html
podman run \
  --rm \
  -it \
  --security-opt label=disable \
  -v "$PWD/..:/mnt" \
  "$image" \
  bash -c 'cd ~; test -r /mnt/.tmux.conf || \
    { echo "FAIL: cannot read /mnt/.tmux.conf"; exit 99; }; \
    ln -s /mnt/.tmux.conf ; \
    ln -s /mnt/.tmux ; yum install -y tmux ; \
    tmux'

# Additional Diagnostic commands (replace `tmux'` in line above):
#
#   tmux run-shell "cat /etc/redhat-release || : ; \
#   echo TMUX_VERSION: $(tmux -V) ||:" || \
#   tmux'
