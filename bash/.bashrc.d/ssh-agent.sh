#!/bin/bash

# directory that contains PID files for each host
export SSH_ENV=${SSH_ENV:-"${HOME}/.ssh/ssh-agent_environments/${HOSTNAME}"}

function _ssh_start_agent {
  echo "Initialising new SSH agent..."
  mkdir -p "$(dirname "${SSH_ENV}")"
  /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
  echo "...Finished initializing SSH agent"
  chmod 600 "${SSH_ENV}"
  . "${SSH_ENV}" > /dev/null
  /usr/bin/ssh-add;
}

function ssh_start_agent {
  # Source SSH settings, if applicable
  echo "== SSH_ENV: '${SSH_ENV}'"

  if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    #ps ${SSH_AGENT_PID} doesn't work under cywgin
    ps -ef | test -S "${SSH_AUTH_SOCK}" || _ssh_start_agent
  else
    echo "No info found at $SSH_ENV; starting a new agent"
    _ssh_start_agent;
  fi

  echo "== SSH_AGENT_PID: '${SSH_AGENT_PID}'"
  echo "== SSH_AUTH_SOCK: '${SSH_AUTH_SOCK}'"
}

# only run on interactive shells
[[ $- == *i* ]] && shopt -q login_shell && { >/dev/null ssh-add -l | wc -l ||  ssh_start_agent ; } # interactive login shell
