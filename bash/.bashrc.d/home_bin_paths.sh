# Add subdirectories under $HOME/bin/*/ to PATH

if [ -d "$HOME/bin" ]; then
  for subpath in $(ls -1dF "$HOME/bin"/* | grep @$ | sed -e 's/@$//'); do
    if [ -d "$(readlink -f "$subpath")" ]; then
      [ "${DEBUG_DOTFILES:-no}" != "no" ] && echo "== \$DEBUG_DOTFILES: .bashrc.d/paths.sh \$PATH += '$subpath'"
      export PATH="$subpath:$PATH" # Add ~/bin to PATH for scripting
    fi
  done
fi
