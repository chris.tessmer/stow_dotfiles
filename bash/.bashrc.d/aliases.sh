export GOPATH=$HOME/src/go
export TERM=xterm-256color       # tmux/vim play nicer with 256 colors

# prevents infinite garbage loops within SIMP codebase
# (e.g.: fixture symlinks loops, .git repos, binary packages, etc)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias igrep='egrep -I -r --exclude-dir=fixtures,bundle,vendor --exclude-dir=.{bundle,git,backup,vendor} --exclude=\*.rpm $*'
alias pupgrep='egrep -r --include=\*.{pp,rb,erb,epp} --exclude=\*_spec.rb $*'
unset -v GREP_OPTIONS

# ssh vars and aliases
# -----------------------------------------------------------------------------
# see .ssh/config for details
alias kwu="ssh kwu"
alias tp="ssh tp"

# proxies
alias kwup="ssh kwup"


# timekeeping helpers
# -----------------------------------------------------------------------------
now()
{
  echo "$(date +%H) + (($(date +%M)*.16) *.1 )" | bc
}

login()
{
   time=$(w | grep tty | awk '{print $4}' | sed -e 's/[a-z]\+//i')
   min=$(echo $time| awk -F: '{print $2}')
   hour=$(echo $time| awk -F: '{print $1}')
  [ "$#" -gt 0 ] && [ $1 == '-v' ] && echo -n "${hour}:${min} = "
   echo "${hour} + ((${min}*.16)*.1)" | bc
}

t()
{
  [ "$#" -gt 0 ] && [ $1 == '-v' ] && echo -n "$(now)-$(login) = "
  echo $(now)-$(login) | bc
}


# git aliases
alias gl='git lg'
alias glf='git lgf'
alias gsu='git status -uno'

alias bbundle='/opt/puppetlabs/bolt/bin/bundle'
alias bgem='/opt/puppetlabs/bolt/bin/gem'
alias bruby='/opt/puppetlabs/bolt/bin/ruby'
alias bpuppet='/opt/puppetlabs/bolt/bin/puppet'

bbbundle()
{
  #hub remote add op-ct ; git fetch op-ct && git checkout update-deps-to-new-systemd-module &&
  /opt/puppetlabs/bolt/bin/bundle config path .vendor && /opt/puppetlabs/bolt/bin/bundle
  if (( $# > 0 )); then
    /opt/puppetlabs/bolt/bin/bundle "$@"
  fi
}


venv()
{
  if [ -f ./venv/bin/activate ]; then
     source venv/bin/activate
   else
     [ -f ~/src/venv/bin/activate ] && source ~/src/venv/bin/activate
   fi
}

