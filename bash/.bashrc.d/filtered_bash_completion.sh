for completion_dir in /usr/share/bash-completion/completions /usr/local/etc/bash_completion.d /etc/bash_completion.d; do
  if [ -n "$BASH_COMPLETION_VERSINFO" ]; then
    [ "${DEBUG_DOTFILES:-no}" != "no" ] && echo "-- \$DEBUG_DOTFILES:    .bashrc.d/filtered_bash_completion.sh SKIPPING '$completion_dir' because \$BASH_COMPLETION_VERSINFO is set to '$BASH_COMPLETION_VERSINFO'"
    continue
  fi

  if [ -d "$completion_dir" ]; then
      for completion_script in "$completion_dir"/*; do

        # don't source autocomplete if the `have` scripts are present;
        # `bash-completion` is probably already doing that.
        #
        # it might be better to test for /usr/share/bash-completion/bash_completion
        if grep -h '\<have\>' "$completion_script" | grep -v '^\s*#' &> /dev/null; then
          [[ "${DEBUG_DOTFILES:-no}" =~ 'completion' ]] && echo "-- \$DEBUG_DOTFILES:   .bashrc.d/filtered_bash_completion.sh skip sourcing '$completion_script' because \<have\>"
          continue
        fi

        # Ignore noisy files
        __fbc_skip_source=n
        for script_to_ignore in yum-utils.bash; do
          [[ "$completion_script" == "${completion_dir}/${script_to_ignore}" ]] && __fbc_skip_source=y
        done
        if [ "$__fbc_skip_source" == y ]; then
          [ "${DEBUG_DOTFILES:-no}" != "no" ] && echo "-- \$DEBUG_DOTFILES:   .bashrc.d/filtered_bash_completion.sh SKIPPING sourcing '$completion_script' because it causes problems"
          continue # skip this script
        fi

        if [ -s "$completion_script" ]; then
          source "$completion_script"
          [[ "${DEBUG_DOTFILES:-no}" =~ 'completion' ]] && echo "-- \$DEBUG_DOTFILES:   .bashrc.d/filtered_bash_completion.sh sourcing '$completion_script'"
        fi
      done
  fi

done
unset -v __fbc_skip_source
