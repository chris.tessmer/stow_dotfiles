#!/bin/sh

HUB_BASH_COMPLETION_SH="${HUB_BASH_COMPLETION_SH:-$HOME/Downloads/hub-linux-amd64-2.10.0/etc/hub.bash_completion.sh}"
if [ -f "${HUB_BASH_COMPLETION_SH}" ]; then
  source "${HUB_BASH_COMPLETION_SH}"
fi
