# https://github.com/puppetlabs/pdkgo
if [ -d "$HOME/.puppetlabs/pct" ]; then
  export PATH="$PATH:$HOME/.puppetlabs/pct"
  # Set up completions
  command -v pct &> /dev/null && source <(pct completion bash)
fi
