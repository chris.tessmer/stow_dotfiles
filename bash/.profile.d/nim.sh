#!/bin/bash
# Add nimble (nim package manager) binaries to PATH
if [ -d "$HOME/.nimble/bin" ]; then
  export PATH="$HOME/.nimble/bin:$PATH"
fi
export PATH
