#!/bin/bash

# Allow tar -cJf to compress .xz using all available CPUs
XZ_DEFAULTS="-T 0"
export XZ_DEFAULTS
