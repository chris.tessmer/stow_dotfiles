# Add Vagrant to PATH for scripting
if [ -d "/opt/${USER}/.vagrant.d" ]; then
  VAGRANT_HOME="/opt/${USER}/.vagrant.d"
  export VAGRANT_HOME
fi
if [ -d "/opt/${USER}/.vagrant" ]; then
  VAGRANT_DOTFILE_PATH="/opt/${USER}/.vagrant"
  export VAGRANT_DOTFILE_PATH
fi
