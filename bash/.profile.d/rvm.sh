# Add RVM to PATH for scripting
if [ -d "/opt/${USER}/.rvm/bin" ]; then
  rvm_prefix="/opt/${USER}"
  rvm_path="/opt/${USER}/.rvm"
  export rvm_prefix
  export rvm_path
elif [ -d "${HOME}/.rvm/bin" ]; then
  rvm_path="${HOME}/.rvm"
fi

[[ -s "${rvm_path}/scripts/rvm" ]] && source "${rvm_path}/scripts/rvm" # Load RVM into a shell session *as a function*
# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
[ -d "${rvm_path}/bin" ] && export PATH="${rvm_path}/bin:$PATH"
export PATH
