# .bash_profile
# --------------------------------------
# This file is run first by bash at login (after /etc/profile)
#
# - https://www.gnu.org/software/bash/manual/bashref.html#Bash-Startup-Files
#

# if the shell is interactive, load other functions and aliases
if [ -f ~/.bashrc ]; then
  [[ $- == *i* ]] && . ~/.bashrc  # `$-` is the current set of shell options
fi

# User specific environment and startup programs

PATH=$PATH:"${HOME}/bin"
export PATH

if [ -n "${rvm_path}" ]; then
  if ! [[ "${rvm_path}" =~ ^/home ]]; then
    # interactive login shell
    [[ $- == *i* ]] && shopt -q login_shell && \
      echo "*** NOTICE: using non-home RVM ($rvm_prefix)"
    rvm reset &> /dev/null # RVM is finicky aobut paths
  fi
fi

[ -f ~/.profile ] && source ~/.profile

if [[ $- == *i* ]] && shopt -q login_shell ; then

  if [[ -n "${VAGRANT_HOME}" ]] && ! [[ "${VAGRANT_HOME}" =~ ^/home ]]; then
    echo "*** NOTICE: using non-home VAGRANT_HOME ($VAGRANT_HOME)"
  fi

  if [[ -n "${VAGRANT_DOTFILE_PATH}" ]] && ! [[ "${VAGRANT_DOTFILE_PATH}" =~ ^/home ]]; then
    echo "*** NOTICE: using non-home VAGRANT_DOTFILE_PATH ($VAGRANT_DOTFILE_PATH)"
  fi
fi
