if [ -d "${HOME}/.profile.d" ]; then
  for i in "${HOME}"/.profile.d/*.sh; do
    [ "${DEBUG_DOTFILES:-no}" != "no" ] && echo == \$DEBUG_DOTFILES: .profile sourcing $i
    source $i
  done
fi
