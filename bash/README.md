
## Structure

```
.
├── .bash_profile
├── .bashrc*
├── .bashrc.d/
│   ├── aliases.sh
│   ├── filtered_bash_completion.sh
│   ├── powerline.sh.disabled
│   ├── ssh-agent.sh
│   └── travis.sh
├── .profile
├── .profile.d/
│   ├── google-chrome-cache.sh
│   ├── rvm.sh
│   └── vagrant.sh
└── .stow-local-ignore
```
