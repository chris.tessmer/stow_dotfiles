# .bashrc

# need to run this first, seemingly
[ "${DEBUG_DOTFILES:-no}" != "no" ] && echo "== \$DEBUG_DOTFILES: .bashrc sourcing '${HOME}/.profile.d/rvm.sh'"
[ -f "${HOME}/.profile.d/rvm.sh" ] && source "${HOME}/.profile.d/rvm.sh"

# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# User specific aliases and functions
if [ -d "${HOME}/.bashrc.d" ]; then
  for i in "${HOME}"/.bashrc.d/*.sh; do
    [ "${DEBUG_DOTFILES:-no}" != "no" ] && echo "== \$DEBUG_DOTFILES: .bashrc sourcing ${i}"
    source "${i}"
  done
fi

if [ -d "${HOME}/bin" ]; then
  export PATH="$HOME/bin:$PATH" # Add ~/bin to PATH for scripting
fi

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
[ -n "${rvm_path}" ] && [ -d "${rvm_path}/bin" ] && export PATH="${rvm_path}/bin:$PATH"
